# IDE Configuration

# Visual code

Please add Workspace Settings:

```
{
  "typescript.tsdk": "node_modules/typescript/lib",
  "typescript.format.insertSpaceBeforeFunctionParenthesis": true,
  "javascript.format.insertSpaceBeforeFunctionParenthesis": true,
  "typescript.format.insertSpaceAfterConstructor": true,
}
```

Reason is to maintain same version of typescript and to keep same syntax when formatting.

# Testing

To run test there is `.env` file needed with content like this:

```
BASE_URL=base_url
KEY=api_key

```

where `base_url` and `api_key` should be proper api information that this library should connect to

# Building

Command to build the library is:

```
yarn build
```

It should build library in lib folder with index.js file that can be used by any other library as dependency in package.json. It should also have TypeScript declarations inside.

# Browser vs. Node.js

To support HTTP Caching [got](https://www.npmjs.com/package/got) is used in Node.js. Since [got](https://www.npmjs.com/package/got) does not work in the browser there is a platform switch to use [superagent]https://github.com/visionmedia/superagent) as alternative.
