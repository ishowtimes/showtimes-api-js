import ShowtimesApiClient from './base';
/**
 * Class for getting information about cinemas, showtimes, chains from Cinepass api
 */
export default class ShowtimesApiSuperagentClient extends ShowtimesApiClient {
    get(route: string, query?: any): Promise<any>;
    head(route: string, query?: any): Promise<any>;
}
