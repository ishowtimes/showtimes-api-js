import { IShowtimesApiConfig } from '..';
import ShowtimesApiClient from './base';
export default class ShowtimesApiGotClient extends ShowtimesApiClient {
    private cache;
    constructor(config: IShowtimesApiConfig);
    get(route: string, query?: any): Promise<any>;
    head(route: string, query?: any): Promise<any>;
    private requestOptions;
}
