import { IShowtimesApiConfig } from '..';
export default abstract class ShowtimesApiClient {
    protected config: IShowtimesApiConfig;
    constructor(config: IShowtimesApiConfig);
    abstract head(route: string, query?: any): Promise<{
        headers: {
            [x: string]: string;
        };
    }>;
    abstract get(route: string, query?: any): Promise<any>;
}
