/**
 * Intercace for ShowtimesApi init configuration
 */
export default interface IShowtimesApiConfig {
    baseUrl: string;
    key: string;
    timeout: number;
}
