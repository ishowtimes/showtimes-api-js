import ShowtimesApiClient from './clients/base';
import IShowtimesApiConfig from './IShowtimesApiConfig';
/**
 * Class for getting information about cinemas, showtimes, chains from Cinepass api
 */
export default class ShowtimesApi {
    private client;
    constructor(config: IShowtimesApiConfig, client?: ShowtimesApiClient);
    getCount(route: string, query?: any): Promise<number>;
    getChains(query: any): Promise<any[]>;
    getCinemas(query?: any): Promise<any[]>;
    getCinemaIds(query?: any): Promise<any[]>;
    getShowtimes(query: any): Promise<any[]>;
    getShowtimesCount(query: any): Promise<number>;
    getCities(query: any): Promise<any[]>;
    getCountries(query?: any): Promise<any[]>;
    getMovies(query: any): Promise<any[]>;
    getMovie(id: string, query: any): Promise<any[]>;
    getAttributes(): Promise<any[]>;
    private fetchList;
}
