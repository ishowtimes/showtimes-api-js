import { expect, should } from 'chai'
import * as dotenv from 'dotenv'
import 'mocha'
import ShowtimesApiGotClient from './clients/got'
import ShowtimesApiSuperagentClient from './clients/superagent'
import IShowtimesApiConfig from './IShowtimesApiConfig'
import ShowtimesApi from './ShowtimesApi'

dotenv.config()

const config: IShowtimesApiConfig = {
  baseUrl: process.env.BASE_URL || '',
  key: process.env.KEY || '',
  timeout: 300000
}

const clients = {
  got: ShowtimesApiGotClient,
  superagent: ShowtimesApiSuperagentClient
}

describe('ShowtimesApi', () => {
  let api: ShowtimesApi
  let apiConfig: IShowtimesApiConfig
  Object.keys(clients).forEach(client => {
    const Client = clients[client]
    context(`with ${client} client`, () => {
      context('with proper api key and small timeout', () => {
        beforeEach(() => {
          apiConfig = { ...config, timeout: 200 }
          api = new ShowtimesApi(apiConfig, new Client(apiConfig))
        })

        context('#getCinemas', () => {
          it('should timeout and have status 408', async () => {
            try {
              await api.getCinemas()
            } catch (error) {
              expect(error).to.be.an('error')
            }
          })
        })
      })

      context('without proper api key', () => {
        beforeEach(() => {
          apiConfig = { ...config, key: '' }
          api = new ShowtimesApi(apiConfig, new Client(apiConfig))
        })

        context('#getCount("cinemas")', () => {
          it('should throw error with 401 status', async () => {
            try {
              await api.getCount('cinemas')
            } catch (error) {
              expect(error).to.be.an('error')
              expect(error.statusCode).to.be.eq(401)
            }
          })
        })

        context('#getCinemas()', () => {
          it('should throw error with 401 status', async () => {
            try {
              await api.getCinemas('cinemas')
            } catch (error) {
              expect(error).to.be.an('error')
              expect(error.statusCode).to.be.eq(401)
            }
          })
        })
      })

      context('with proper api key', () => {
        beforeEach(() => {
          api = new ShowtimesApi(config, new Client(config))
        })

        context('#getCount', () => {
          it('should get count of cinemas route without query', async () => {
            const count = await api.getCount('cinemas')
            expect(count).to.be.a('number')
            expect(count).to.be.at.least(0)
          })
        })

        context('#getCinemas', () => {
          it('should fetch cinemas array without query', async () => {
            const res = await api.getCinemas({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.include.all.keys('id', 'name', 'location')
          })
        })

        context('#getChains', () => {
          it('should fetch chains array without query', async () => {
            const res = await api.getChains({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.include.all.keys('id', 'name')
          })
        })

        context('#getCinemaIds', () => {
          it('should fetch cinemaIds array without query', async () => {
            const res = await api.getCinemaIds({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.be.a('string')
          })
        })

        context('#getShowtimesCount', () => {
          it('should get showtimes count without query', async () => {
            const res = await api.getShowtimesCount({})
            expect(res).to.not.equal(null)
            expect(res).to.be.a('number')
            expect(res).to.equal(0)
          })
        })

        context('#getCities', () => {
          it('should fetch cities array without query', async () => {
            const res = await api.getCities({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.include.all.keys('id', 'name', 'country')
          })
        })

        context('#getCountries', () => {
          it('should fetch countries array without query', async () => {
            const res = await api.getCountries({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.include.all.keys('iso_code')
          })
        })

        context('#getMovies', () => {
          it('should fetch movies array without query', async () => {
            const res = await api.getMovies({})
            expect(res).to.not.equal(null)
            expect(res).to.be.an('array')
            expect(res).to.have.lengthOf.at.least(1)
            const element = res[0]
            expect(element).to.include.all.keys('id', 'title')
          })
        })

        context('#getAttributes', () => {
          it('should fetch movie attributes', async () => {
            const attributes = await api.getAttributes()
            expect(attributes).to.not.equal(null)
            expect(attributes).to.be.an('array')
            expect(attributes).to.have.lengthOf.at.least(1)
            expect(attributes[0]).to.include.all.keys('code', 'title', 'description')
          })
        })
      })
    })
  })
})
