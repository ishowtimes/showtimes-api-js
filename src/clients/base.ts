import { IShowtimesApiConfig } from '..'

export default abstract class ShowtimesApiClient {
  constructor(protected config: IShowtimesApiConfig) {}
  public abstract head(route: string, query?: any): Promise<{ headers: { [x: string]: string } }>
  public abstract get(route: string, query?: any): Promise<any>
}
