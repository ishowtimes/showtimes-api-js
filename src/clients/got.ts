import * as got from 'got'
import { IShowtimesApiConfig } from '..'
import ShowtimesApiClient from './base'

export default class ShowtimesApiGotClient extends ShowtimesApiClient {
  private cache: Map<any, any>

  constructor(config: IShowtimesApiConfig) {
    super(config)
    this.cache = new Map()
  }

  public async get(route: string, query: any = null) {
    const res = await got(route, this.requestOptions(query))
    return res.body
  }

  public async head(route: string, query: any = null) {
    const res = await got.head(route, this.requestOptions(query))
    return res
  }

  private requestOptions(query: any = null) {
    return {
      baseUrl: this.config.baseUrl,
      cache: this.cache,
      headers: {
        Accept: 'application/json',
        'X-API-Key': this.config.key
      },
      json: true,
      query: new URLSearchParams(query),
      timeout: this.config.timeout
    }
  }
}
