import * as retryProxy from 'async-retry'
import * as superagentProxy from 'superagent'
import * as UrlParser from 'url'
import * as urljoinProxy from 'url-join'
import ShowtimesApiClient from './base'

const retry = (retryProxy as any).default || retryProxy
const urljoin = (urljoinProxy as any).default || urljoinProxy
const superagent = (superagentProxy as any).default || superagentProxy

/**
 * Class for getting information about cinemas, showtimes, chains from Cinepass api
 */

export default class ShowtimesApiSuperagentClient extends ShowtimesApiClient {
  public async get(route: string, query: any = null) {
    const url = urljoin(this.config.baseUrl, route)

    const logUrl = UrlParser.parse(url)
    logUrl.query = query
    return retry(
      async bail => {
        try {
          const res = await superagent
            .get(url)
            .query(query)
            .set('X-API-Key', this.config.key)
            .set('Accept', 'application/json')
            .timeout(this.config.timeout)
            .ok(response => response.status < 500)

          if (res && (res.status === 403 || res.status === 401)) {
            throw {
              body: res.body || {},
              headers: res.headers || {},
              status: res.status
            }
          }
          return res
        } catch (err) {
          if (err && (err.status === 401 || err.status === 403)) {
            return { error: new Error('Unauthorized'), body: err.body, status: err.status, headers: err.headers }
          } else if (err.timeout === this.config.timeout && err.code === 'ECONNABORTED' && err.errno === 'ETIME') {
            return { error: err.code, body: {}, status: 408 }
          }
          return { error: err, body: {}, status: 500 }
        }
      },
      {
        maxTimeout: this.config.timeout * 5,
        retries: 5
      }
    )
  }

  public async head(route: string, query: any = null) {
    const url = urljoin(this.config.baseUrl, route)

    const logUrl = UrlParser.parse(url)
    logUrl.query = query
    return retry(
      async bail => {
        try {
          const res = await superagent
            .head(url)
            .query(query)
            .set('X-API-Key', this.config.key)
            .type('text/plain')
            .ok(sResponse => sResponse.status < 500)

          if (res && (res.status === 403 || res.status === 401)) {
            throw {
              body: res.body || {},
              headers: res.headers || {},
              status: res.status
            }
          }
          return res
        } catch (err) {
          if (err && (err.status === 401 || err.status === 403)) {
            return { error: new Error('Unauthorized'), body: err.body, status: err.status, headers: err.headers }
          } else if (err.timeout === this.config.timeout && err.code === 'ECONNABORTED' && err.errno === 'ETIME') {
            return { error: err.code, body: {}, status: 408 }
          }
          return { error: err, body: {}, status: 500 }
        }
      },
      {
        maxTimeout: this.config.timeout * 5,
        retries: 5
      }
    )
  }
}
