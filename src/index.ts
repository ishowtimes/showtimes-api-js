/**
 * Module from exporting ShowtimesApi to be used as dependency in other libraries
 */

import IShowtimesApiConfig from './IShowtimesApiConfig'
import ShowtimesApi from './ShowtimesApi'

export { IShowtimesApiConfig }
export default ShowtimesApi
