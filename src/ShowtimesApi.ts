import { isNode } from 'browser-or-node'
import ShowtimesApiClient from './clients/base'
import ShowtimesApiGotClient from './clients/got'
import ShowtimesApiSuperagentClient from './clients/superagent'
import IShowtimesApiConfig from './IShowtimesApiConfig'

/**
 * Class for getting information about cinemas, showtimes, chains from Cinepass api
 */

export default class ShowtimesApi {
  private client: ShowtimesApiClient
  constructor(config: IShowtimesApiConfig, client?: ShowtimesApiClient) {
    this.client = client || isNode ? new ShowtimesApiGotClient(config) : new ShowtimesApiSuperagentClient(config)
  }

  public async getCount(route: string, query: any = null): Promise<number> {
    const res = await this.client.head(route, query)
    return parseInt(res.headers['x-total-count'], 10) || 0
  }

  public async getChains(query: any): Promise<any[]> {
    return this.fetchList('chains', query)
  }

  public async getCinemas(query: any = null): Promise<any[]> {
    return this.fetchList('cinemas', query)
  }

  public async getCinemaIds(query: any = {}) {
    query.fields = 'id'
    const cinemas = await this.getCinemas(query)
    return cinemas.map((cinema: any) => cinema.id)
  }

  public async getShowtimes(query: any): Promise<any[]> {
    return this.fetchList('showtimes', query)
  }

  public async getShowtimesCount(query: any): Promise<number> {
    return this.getCount('showtimes', query)
  }

  public async getCities(query: any): Promise<any[]> {
    return this.fetchList('cities', query)
  }

  public async getCountries(query: any = null): Promise<any[]> {
    return this.fetchList('countries', query)
  }

  public async getMovies(query: any): Promise<any[]> {
    return this.fetchList('movies', query)
  }

  public async getMovie(id: string, query: any): Promise<any[]> {
    return this.client.get(`movies/${id}`, query)
  }

  public async getAttributes(): Promise<any[]> {
    return this.fetchList('attributes')
  }

  private async fetchList<T>(key, query: any = null): Promise<T[]> {
    const res = await this.client.get(key, query)
    return res[key]
  }
}
