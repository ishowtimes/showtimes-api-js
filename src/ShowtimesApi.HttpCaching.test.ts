import { expect } from 'chai'
import * as dotenv from 'dotenv'
import * as express from 'express'
import { Server } from 'http'
import 'mocha'
import ShowtimesApiGotClient from './clients/got'
import ShowtimesApiSuperagentClient from './clients/superagent'
import IShowtimesApiConfig from './IShowtimesApiConfig'
import ShowtimesApi from './ShowtimesApi'

dotenv.config()

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const port = 3333

let requestCounter = 0

function startMochServer (cacheControl?: string) {
  const app = express()
  app.get('/v4/cities', (_, res) => {
    requestCounter += 1
    process.env.VERBOSE && console.log(new Date(), '/v4/cities', requestCounter)

    if (cacheControl) {
      res.set('Cache-Control', cacheControl)
    }

    res.send({
      cities: [
        {
          id: '1',
          name: 'Berlin'
        }
      ]
    })
  })

  return app.listen(port, () => {
    process.env.VERBOSE && console.log(`mock server app listening on port ${port}!`)
  })
}


const config: IShowtimesApiConfig = {
  baseUrl: `http://localhost:${port}/v4`,
  key: 'not-needed',
  timeout: 300000
}

const clients = {
  got: ShowtimesApiGotClient,
  superagent: ShowtimesApiSuperagentClient
}

describe('ShowtimesApi / HTTP Caching', () => {
  let api: ShowtimesApi
  let mockApiServer: Server
  Object.keys(clients).forEach((client) => {
    const Client = clients[client]
    context(`with ${client} client`, () => {

      context('without Cache-Control header', () => {
        beforeEach(() => {
          api = new ShowtimesApi(config, new Client(config))
          requestCounter = 0
          mockApiServer = startMochServer()
        })

        it('should make 2 requests', async () => {
          await api.getCities({})
          await api.getCities({})
          expect(requestCounter).to.equal(2)
        })

        after(() => {
          mockApiServer.close()
        })
      })

      const testWithCaching = (cacheControl: string) => {
        const match = cacheControl.match(/max-age=(\d+)/)
        const maxAge = +match![1]
        let results

        before(async () => {
          results = []
          api = new ShowtimesApi(config, new Client(config))
          requestCounter = 0
          mockApiServer = startMochServer(cacheControl)

          // prepare by making all the requests
          let cities = await api.getCities({})
          results.push(cities)
          cities = await api.getCities({})
          results.push(cities)
        })

        it('should make only 1 requests during max-age time', () => {
          expect(requestCounter).to.equal(1)
        })

        it('should make fetched cities twice', () => {
          expect(results).to.have.lengthOf(2)
          expect(results[0]).to.deep.equal(results[1])
        })

        context('after the max-age time is over', () => {
          it(`wait for ${maxAge}.5 seconds`, async () => {
            await sleep(maxAge * 1000 + 500)
          })

          it('should make another request', async () => {
            await api.getCities({})
            expect(requestCounter).to.equal(2)
          })

          it('should still fetch the data', async () => {
            const cities = await api.getCities({})
            expect(cities[0].name).to.equal(results[0][0].name)
          })
        })

        after(() => {
          mockApiServer.close()
        })
      }

      context('with Cache-Control = max-age=3', () => {
        testWithCaching(`max-age=3`)
      })


      context('with Cache-Control = max-age=3, public', () => {
        testWithCaching(`max-age=3, public`)
      })
    })
  })

})
